<?php

Route::get('/', 'FavoritosController@inicio');
Route::post('/categoria/novo', 'CategoriasController@novo');
Route::post('/categoria/deleta/{id}', 'CategoriasController@deleta');
Route::get('/{categoria}', 'FavoritosController@lista');
Route::post('/{categoria_id}/novo', 'FavoritosController@novo');
Route::post('/{categoria_id}/deleta', 'FavoritosController@deleta');
Route::get('/{categoria}/{favorito}', 'FavoritosController@mostra');
