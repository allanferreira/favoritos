<form class="insereCategoria" method="post" action="/categoria/novo" :class="{ aberto : adicionaCategoria }">
  {{ csrf_field() }}
  <div class="icone">+</div>
  <input type="text" name="categoria" v-model="adicionaCategoria">
</form>
