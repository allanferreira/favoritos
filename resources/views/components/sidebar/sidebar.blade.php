<nav class="sidebar">
  <ul>
    <li class="sidebar--home">
      <a href="/">Favoritos</a>
    </li>
    <?php $i=0 ?>
    @foreach($categorias as $c)
      <li>
        <a href="/{{$c->slug}}">
          {{$c->nome}}
          @if($favoritosPorcategoria[$i])
            <span>
              {{$favoritosPorcategoria[$i]}}
            </span>
          @endif
        </a>
      </li>
      <?php $i++ ?>
    @endforeach
    @include('components.insereCategoria.insereCategoria')
  </ul>
</nav>
