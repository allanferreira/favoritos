<table class="table">
  <thead>
    <tr>
      <td class="table--nome">Nome</td>
      <td class="table--url">Link</td>
      <td>Data</td>
      <td class="table--acao">Ação</td>
    </tr>
  </thead>
  <tbody>
    {{ $slot }}
  </tbody>
</table>
