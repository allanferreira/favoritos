@extends('components.app.app')
@section('conteudo')


<h1>
    Favoritos de <span class="h--cvermelho">{{$c->nome}}</span>
    @include('components.deletaCategoria.deletaCategoria')
</h1>

@include('components.insereFavorito.insereFavorito')

@if(count($favoritos) !== 0)
  @component('components.table.table')
    @foreach($favoritos as $f)

      <tr>
        <td class="table--nome">
          <a href="/{{$c->slug}}/{{$f->slug}}">{{$f->nome}}</a>
        </td>

        <td class="table--url">
          <a href="{{$f->url}}" target="_blank">{{$f->url}}</a>
        </td>

        <td>23/05/2017</td>

        <td class="table--acao">
          <form method="post" action="/{{$f->id}}/deleta">
            {{ csrf_field() }}
            <input type="hidden" name="categoria_slug" value="{{$c->slug}}">
            <button type="submit">Remover</button>
          </form>
        </td>
      </tr>

    @endforeach
  @endcomponent
@else
  Ainda não tem favoritos cadastrados nessa categoria
@endif

@endsection
