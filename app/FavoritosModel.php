<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FavoritosModel extends Model {
    function insere($favorito){
        DB::table('favoritos')->insert($favorito);
    }
    function remove($id){
        DB::table('favoritos')->where('id', $id)->delete();
    }
    function listaCategorias(){
        return DB::table('categorias')->get();
    }
    function favoritoExiste($favorito){
        return DB::table('favoritos')->where('slug',$favorito)->first();
    }
    function categoriaExiste($categoria){
        return $this->pegaCategoria($categoria)->first();
    }
    function pegaFavorito($favorito){
        return DB::table('favoritos')->where('slug',$favorito);
    }
    function pegaCategoria($categoria){
        return DB::table('categorias')->where('slug',$categoria);
    }
    function favoritosEm($categoria){
        $categoria_id = $this->pegaCategoria($categoria)->first()->id;
        return DB::table('favoritos')->where('categoria_id',$categoria_id)->get();
    }
    function totalFavoritos($categoriasLista){
        $totalFavoritos = [];

        foreach ($categoriasLista as $c) {
            $id = $c->id;
            $favoritosNaCategoria = DB::table('favoritos')->where('categoria_id',$id)->count();
            $totalFavoritos[] += $favoritosNaCategoria;
        }

        return $totalFavoritos;
    }
}
