<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CategoriasModel extends Model{
    function insere($categoria){
        DB::table('categorias')->insert($categoria);
    }
    function remove($id){
        DB::table('categorias')->where('id', $id)->delete();
    }
}
