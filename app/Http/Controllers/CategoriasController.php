<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoriasModel;

class CategoriasController extends Controller {
    public function novo(Request $request){

        $input = $request->all();
        $categoriaSlug = str_slug($input['categoria']);

        $categoria = [
          "nome" => "{$input['categoria']}",
          "slug" => "$categoriaSlug"
        ];

        $categorias = new CategoriasModel;
        $categorias->insere($categoria);

        return redirect( url()->previous() );

    }
    public function deleta(Request $request, $id){

        $input = $request->all();

        $categorias = new CategoriasModel;
        $categorias->remove($id);

        return redirect("/");

    }
}
