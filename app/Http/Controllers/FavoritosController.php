<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FavoritosModel;

class FavoritosController extends Controller {
    public function inicio(){

        $favoritos = new FavoritosModel;
        $categoriasLista = $favoritos->listaCategorias();

        return view('home')
            ->with('categorias',$categoriasLista)
            ->with('favoritosPorcategoria',$favoritos->totalFavoritos($categoriasLista));

    }
    public function lista(Request $request, $categoria){

        $favoritos = new FavoritosModel;
        $categoriasLista = $favoritos->listaCategorias();

        if(!$favoritos->categoriaExiste($categoria)){
            return view('categoriaNaoEncontrada')
                ->with('categorias',$categoriasLista)
                ->with('favoritosPorcategoria',$favoritos->totalFavoritos($categoriasLista));
        }

        return view('favoritos')
            ->with('favoritos',$favoritos->favoritosEm($categoria))
            ->with('c',$favoritos->pegaCategoria($categoria)->first())
            ->with('categorias',$categoriasLista)
            ->with('favoritosPorcategoria',$favoritos->totalFavoritos($categoriasLista));

    }
    public function mostra(Request $request, $categoria, $favorito){

        $favoritos = new FavoritosModel;
        $categoriasLista = $favoritos->listaCategorias();

        if(!$favoritos->favoritoExiste($favorito))
            return redirect("/$categoria");

        return view('favorito')
          ->with('f',$favoritos->pegaFavorito($favorito)->first())
          ->with('c',$favoritos->pegaCategoria($categoria)->first())
          ->with('categorias',$categoriasLista)
          ->with('favoritosPorcategoria',$favoritos->totalFavoritos($categoriasLista));

    }
    public function novo(Request $request, $categoria_id){

        $input = $request->all();
        $nomeSlug = str_slug($input['nome']);

        $favorito = [
          "nome" => "{$input['nome']}",
          "url" => "{$input['url']}",
          "descricao" => "",
          "slug" => "$nomeSlug",
          "categoria_id" => $categoria_id
        ];

        $favoritos = new FavoritosModel;
        $favoritos->insere($favorito);

        return redirect("/{$input['categoria_slug']}");

    }
    public function deleta(Request $request, $favorito_id){

        $input = $request->all();

        $favoritos = new FavoritosModel;
        $favoritos->remove($favorito_id);

        return redirect("/{$input['categoria_slug']}");

    }

}
